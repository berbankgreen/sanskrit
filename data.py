types = [
    "Nominative (subjet)", "Vocative (hey!)", "Accusative (object)",
    "Instrumental (by/with)", "Dative (for)", "Ablative (from)",
    "Genitve (of)", "Locative (in/on)"
]
gender = ["male", "female", "neuter"]
number = ["1", "2", "3"]
name_ending = ["a"]

names = [
    "rAma:",
    "sitA",
    "vanam",
]


def get_key(type_index, gender_index, number_index, ending_index=0):
    return types[type_index] + "|" + gender[gender_index] + "|" + number[
        number_index] + "|" + name_ending[ending_index]


declention = {
    # nominative (subject)
    # male
    get_key(0, 0, 0): "-:",
    get_key(0, 0, 1): "--au",
    get_key(0, 0, 2): "--A:",
    # female
    get_key(0, 1, 0): "-A",
    get_key(0, 1, 1): "-e",
    get_key(0, 1, 2): "-A:",
    # neuter
    get_key(0, 2, 0): "--am",
    get_key(0, 2, 1): "--e",
    get_key(0, 2, 2): "--Ani",

    # vocative (o!)
    # male
    get_key(1, 0, 0): "-",
    get_key(1, 0, 1): "--au",
    get_key(1, 0, 2): "--A:",
    # female
    get_key(1, 1, 0): "-e",
    get_key(1, 1, 1): "-e",
    get_key(1, 1, 2): "-A:",
    # neuter
    get_key(1, 2, 0): "-",
    get_key(1, 2, 1): "--e",
    get_key(1, 2, 2): "--Ani",

    # Accustaive (object)
    # male
    get_key(2, 0, 0): "-m",
    get_key(2, 0, 1): "--au",
    get_key(2, 0, 2): "--An",
    # female
    get_key(2, 1, 0): "-Am",
    get_key(2, 1, 1): "-e",
    get_key(2, 1, 2): "-A:",
    # neuter
    get_key(2, 2, 0): "--am",
    get_key(2, 2, 1): "--e",
    get_key(2, 2, 2): "--Ani",

    # Instramental (with/using)
    # male
    get_key(3, 0, 0): "--ena",
    get_key(3, 0, 1): "--AbhyAm",
    get_key(3, 0, 2): "--ai:",
    # female
    get_key(3, 1, 0): "-ayA",
    get_key(3, 1, 1): "-AbhyAm",
    get_key(3, 1, 2): "-Abhi:",
    # neuter
    get_key(3, 2, 0): "--ena",
    get_key(3, 2, 1): "--AbhyAm",
    get_key(3, 2, 2): "--ai:",

    # Dative (for)
    # male
    get_key(4, 0, 0): "--Aya",
    get_key(4, 0, 1): "--AbhyAm",
    get_key(4, 0, 2): "--ebhya:",
    # female
    get_key(4, 1, 0): "-Ayai",
    get_key(4, 1, 1): "-AbhyAm",
    get_key(4, 1, 2): "-Abhya:",
    # neuter
    get_key(4, 2, 0): "--Aya",
    get_key(4, 2, 1): "--AbhyAm",
    get_key(4, 2, 2): "--ebhya:",

    # Ablative (for)
    # male
    get_key(5, 0, 0): "--At",
    get_key(5, 0, 1): "--AbhyAm",
    get_key(5, 0, 2): "--ebhya:",
    # female
    get_key(5, 1, 0): "-AyA:",
    get_key(5, 1, 1): "-AbhyAm",
    get_key(5, 1, 2): "-Abhya:",
    # neuter
    get_key(5, 2, 0): "--At",
    get_key(5, 2, 1): "--AbhyAm",
    get_key(5, 2, 2): "--ebhya:",

    # Genative (of/belongs to)
    # male
    get_key(6, 0, 0): "--asya",
    get_key(6, 0, 1): "--ayo:",
    get_key(6, 0, 2): "--AnAm",
    # female
    get_key(6, 1, 0): "-AyA:",
    get_key(6, 1, 1): "-ayo:",
    get_key(6, 1, 2): "-AnAm",
    # neuter
    get_key(6, 2, 0): "--asya",
    get_key(6, 2, 1): "--ayo:",
    get_key(6, 2, 2): "--AnAm",

    # Locative (in/on)
    # male
    get_key(7, 0, 0): "--e",
    get_key(7, 0, 1): "--ayo:",
    get_key(7, 0, 2): "--es.u",
    # female
    get_key(7, 1, 0): "-Ayam",
    get_key(7, 1, 1): "-ayo:",
    get_key(7, 1, 2): "-Asu",
    # neuter
    get_key(7, 2, 0): "--e",
    get_key(7, 2, 1): "--ayo:",
    get_key(7, 2, 2): "--es.u",
}