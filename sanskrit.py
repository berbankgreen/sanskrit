import random
import data

user_input = "default"

def get_gender_index(name):
    name_gender = data.gender[0]
    if name[-1] == ":":
        name_gender = data.gender[0]
        return 0
    elif name[-1] == "A":
        name_gender = data.gender[1]
        return 1
    elif name[-1] == "m":
        name_gender = data.gender[2]
        return 2

def change_declention(name, ending):
    # gender_index = get_gender_index(name)
    count = ending.count("-")
    stripped = ending.replace("-", "")
    return name[0:-count] + stripped

def get_key_info(declention_key, index):
    key_split = declention_key.split("|")
    return key_split[index]


def get_subset(types, genders, numbers):
    subset = {}
    for type_index in types:
        for gender_index in genders:
            for number_index in numbers:
                key = data.get_key(type_index, gender_index, number_index)
                subset[key] = data.declention[key]
    return subset
correct = True
while user_input != "":

    # repeat if incorrect guess
    if correct == True: 
        # type 0-8, gender 0-3, number 0-3
        a_set = get_subset(range(0,8),range(0,3), range(0,3))
        a_key = random.choice(list(a_set))
        # a_key = random.choice(list(declention))

    key_info = a_key.split("|")
    name = data.names[data.gender.index(key_info[1])]
    answer = change_declention(name, data.declention[a_key])

    key_data = a_key.split("|")
    question = key_data[2] + " " + key_data[0]
    print(question)

    default_value = name
    user_input = input(name + " : ")

    # _input = input( name + " ? : ")
    stripped = data.declention[a_key].replace("-", "")
    print("")
    if user_input == answer:
        print("yes! it's '" + answer + "'")
        correct = True
    else:
        print("")
        correct = False
        for key, value in a_set.items() :
            # print(value)
            set_info = key.split("|")
            gender_index = get_gender_index(name)
            if set_info[1] == data.gender[gender_index]:
                set_answer = change_declention(name, value)
                print(set_info[2] + " " + set_info[0] + " :" + set_answer)
        print("")
        print("not " + user_input + " , it's '" + answer + "'" + " for " + question)
    print("")
    print("")
